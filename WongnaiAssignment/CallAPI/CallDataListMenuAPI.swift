//
//  CallDataListMenuAPI.swift
//  WongnaiAssignment
//
//  Created by toffee on 9/1/2563 BE.
//  Copyright © 2563 toffee. All rights reserved.
//

import UIKit
import Alamofire

protocol CallDataListMenuAPIDelegate {
    func callDataListMenuAPI(class:CallDataListMenuAPI,data:[LMListMenuModel],success:Bool)
}
class CallDataListMenuAPI: NSObject{
    var delegate:CallDataListMenuAPIDelegate?
    var str:String = ""
    var dataArray = [LMListMenuModel]()
    func callData(page:Int){
        AF.request("https://api.500px.com/v1/photos?feature=popular&page=\(page)").responseJSON{ (response) in
         switch response.result {
         case .success(let value):
            let response = value as! NSDictionary
            let data = response.object(forKey: "photos") as! NSArray
                      
                for i in 0...data.count-1{
                            
                    var name:String = ""
                    var description:String = ""
                    var urlImage:String = ""
    
                    let dic = data[i] as! NSDictionary
                            
                    if dic.object(forKey: "name") != nil {
                        name = dic.object(forKey: "name") as! String
                    }
                    else{
                        name = " "
                    }
                            
                    if dic.object(forKey: "description") != nil {
                        description = dic.object(forKey: "description") as! String
                    }
                    else{
                        description = " "
                    }
                            
                    if dic.object(forKey: "image_url") != nil {
                        urlImage = (dic.object(forKey: "image_url") as! NSArray)[0] as! String
                    }
                    else{
                        urlImage = " "
                    }
                            
                    let model = LMListMenuModel()
                    model.nameShop = name as NSString
                    model.descriptionsShop = description as NSString
                    model.urlImageShop = urlImage as NSString
                    self.dataArray.append(model)
                            
                }
                    self.delegate?.callDataListMenuAPI(class: self, data: self.dataArray, success: true)
                case .failure(let error): break
                    self.delegate?.callDataListMenuAPI(class: self, data: self.dataArray, success: false)}

        }
    }
    
    

}
