//
//  LMListMenuTableViewCell.swift
//  WongnaiAssignment
//
//  Created by toffee on 10/1/2563 BE.
//  Copyright © 2563 toffee. All rights reserved.
//

import UIKit

class LMListMenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var shopImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var amountLikeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
