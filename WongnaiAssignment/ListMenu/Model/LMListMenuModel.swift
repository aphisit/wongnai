//
//  LMListMenuModel.swift
//  WongnaiAssignment
//
//  Created by toffee on 10/1/2563 BE.
//  Copyright © 2563 toffee. All rights reserved.
//

import UIKit

class LMListMenuModel: NSObject {
  private  var name:NSString = ""
  private  var descriptions:NSString = ""
  private  var urlImage:NSString = ""
    
    
    var nameShop: NSString {
        get {
            return name
        }
        set(value) {
            name = value
        }
    }
    
    var descriptionsShop: NSString {
           get {
               return descriptions
           }
           set(value) {
               descriptions = value
           }
       }
    
    var urlImageShop: NSString {
        get {
            return urlImage
        }
        set(valueee) {
            urlImage = valueee
        }
    }
}
