//
//  LMListMenuViewController.swift
//  WongnaiAssignment
//
//  Created by toffee on 9/1/2563 BE.
//  Copyright © 2563 toffee. All rights reserved.
//
 
import UIKit
import Alamofire

class LMListMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CallDataListMenuAPIDelegate  {
    
    @IBOutlet weak var tableView: UITableView!
    
    let callDataListMenuAPI = CallDataListMenuAPI()
    var dataList = [LMListMenuModel]()
    var sectionArray = [Int]()
    var pageNumber = 1
    var sumAllData = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //tableView Nib
        self.tableView.register(UINib(nibName: "ImageInsertionTableViewCell", bundle: nil), forCellReuseIdentifier: "InsertionCell")
        self.tableView.register(UINib(nibName: "LMListMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
       
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 100.0;
        self.tableView.rowHeight = UITableView.automaticDimension;
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.separatorInset = UIEdgeInsets.zero;
        getDataListAPI(page: pageNumber)
    }
    
    // MARK: CallDataListMenuAPI
    func getDataListAPI(page:Int) {
        callDataListMenuAPI.delegate = self
        callDataListMenuAPI.callData(page: page)
    }
    
    func callDataListMenuAPI(class: CallDataListMenuAPI, data: [LMListMenuModel], success: Bool) {
        if success {
            sumAllData = sumAllData + data.count
            for i in 0...data.count-1{
                dataList.append(data[i])
            }
        }
        self.tableView.reloadData()
    }
        
    func numberOfSections(in tableView: UITableView) -> Int {
        dataList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ((section + 1) % 4) == 0 {
            return 2
        }else{
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cellDetail : LMListMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! LMListMenuTableViewCell
            let model:LMListMenuModel = dataList[indexPath.section]
            cellDetail.amountLikeLabel.text = "\(indexPath.section + 1)"
            cellDetail.nameLabel.text = model.nameShop as String
            if !(model.descriptionsShop as String == "") {
                cellDetail.descriptionLabel.text = model.descriptionsShop as String
            }else{
                cellDetail.descriptionLabel.text = " "
            }
            let url = URL(string: model.urlImageShop as String)
            let imageData = try? Data(contentsOf: url!)
            if let data = imageData {
                cellDetail.shopImage.image = UIImage(data: data)
            }
            return cellDetail
        }else{
            let cellInsertImage : ImageInsertionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InsertionCell", for: indexPath) as! ImageInsertionTableViewCell
            return cellInsertImage
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if sumAllData == indexPath.section+1 {
            pageNumber+=1
            getDataListAPI(page: pageNumber)
        }
    }


}
